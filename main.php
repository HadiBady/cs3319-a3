<!DOCTYPE html>
<!-- The code used in all of the files uploaded for this assignment are from the workshop php and the flipped4 classroom -->
<html>
<head>
<meta charset="utf-8">
<title>CS3319 Assignment 3</title>
</head>
<body>


<!-- introduction to my website -->
<h1>Welcome to the Doctor Database</h1>

<!-- Query option to have the person query all the doctors and sort them by first or last name, and ascending or decending -->

<h2>Doctor Query</h2>
<form action="get_doctor.php" method="post" enctype="multipart/form-data" >
	<fieldset id="name">
		<input type="radio" value="firstname" name="name">First Name<br>
		<input type="radio" value="lastname" name="name">Last Name<br>
	</fieldset>
	<fieldset id="order">	
		<input type="radio" value="ASC" name="order">Ascending<br>
		<input type="radio" value="DESC" name="order">Decending<br>
	</fieldset>
	<input type="submit" value="Query Time!">
</form>
<p>

<!-- attacking second bullet point of assignment where we search by license date certification -->

<hr>
<p>
<h2>Doctor Query by License Date</h2>
<form action="get_doc_by_lic_date.php" method="post">
	<input type="date" name="licensedate">
	<input type="submit">
</form>

<p>
<hr>

<!-- adding a doctor to the database -->


<p>
<h2>Insert New Doctor</h2>
<form action="add_new_doc.php" method="post">
	First Name: <input type="text" name="firstname"><br>
	Last Name: <input type="text" name="lastname"> <br>
	Specialty: <input type="text" name="specialty"><br>
	License Date: <input type="date" name="licensedate"><br>
	License Num: <input type="text" name="licensenum"><br>
	Image URL: <input type="text" name="url"><br>
	Hospital: 
	<br>
		<?php
			include 'connecttodb.php';
			include 'list_hospitals.php';
		?>
	<input type="submit" value="Insert Doctor"> <br>
</form>

<p>
<hr>

<!-- deleting a doctor from the database -->

<p>
<h2>Remove Doctor</h2>
<?php
include 'connecttodb.php';
?>
<h1>List of Doctors:</h1>
<form action="delete.php" method="post">

<ol>

<!-- inserting php code to have the doctors displayed to be removed -->

<?php

   $query = 'SELECT * FROM doctor';
   $result=mysqli_query($connection,$query);
    if (!$result) {
         die("database query2 failed.");
     }
    while ($row=mysqli_fetch_assoc($result)) {
        echo '<li>';
        echo '<input type="radio" name="doctor" value="';
        echo $row["licnum"];
        echo '">' . $row["firstname"] . " " . $row["lastname"] . "<br>";
     }
     mysqli_free_result($result);
?>
</ol>
<input type="submit" value="Remove doctor">
</form>
<?php
   mysqli_close($connection);
?>
<hr>

<!-- updating an already existing hospital name -->

<p>
<h2>Update a Hospital's Name</h2>
<form action="new_hospital_name.php" method="post">
	Select Hospital:<br> 
	<?php
		include'connecttodb.php';
		include'list_hospitals.php';
	?>
	<br>
	New Hospital Name: <input type="text" name="newname"><br>	
	<input type="submit" value="Update"><br>
</form>
<br>
<p>
<hr>

<!-- Listing all the current Head Doctors -->

<p>
<h2>Current Head Doctors</h2>
<?php
	include'connecttodb.php';
	include'head_doc_list.php';
?>
<p>
<hr>

<!-- allowing the user to get the patient information by entering in the ohip number -->

<p>
<h2>Patient Information</h2>
<form action="get_patient_info.php" method="post" enctype="multipart/form-data">
	Enter Patient OHIP Number: <input type="text" name="ohip"><br>
	<input type="submit" value="submit"><br>
</form>
<p>
<hr>

<!-- Allowing the user to remove or make treatments between doctors and patients -->

<p>
<h2>Treatment/No treatment</h2>

<h2>Treatment</h2>
<form action="treatment.php" method="post">
<ol>
<?php
   include 'connecttodb.php';
   echo 'Select Doctor:';
   $query = 'SELECT * FROM doctor';
   $result=mysqli_query($connection,$query);
    if (!$result) {
         die("database query2 failed.");
     }
    while ($row=mysqli_fetch_assoc($result)) {
        echo '<li>';
        echo '<input type="radio" name="doctor" value="';
        echo $row["licnum"];
        echo '">' . $row["firstname"] . " " . $row["lastname"] . "<br>";
     }
     mysqli_free_result($result);
?>
</ol>
<ol>
<?php

   echo 'Select Patient:';
   $query = 'SELECT * FROM patient';
   $result=mysqli_query($connection,$query);
    if (!$result) {
         die("database query2 failed.");
     }
    while ($row=mysqli_fetch_assoc($result)) {
        echo '<li>';
        echo '<input type="radio" name="patient" value="';
        echo $row["ohip"];
        echo '">' . $row["firstname"] . " " . $row["lastname"] . "<br>";
     }
     mysqli_free_result($result);

?>
</ol>
<input type="submit" value="Accept Treatment">
</form>



<h2>No treatment</h2>
<form action="no_treatment.php" method="post">
<ol>
<?php
   echo 'Select Doctor:';
   $query = 'SELECT * FROM doctor';
   $result=mysqli_query($connection,$query);
    if (!$result) {
         die("database query2 failed.");
     }
    while ($row=mysqli_fetch_assoc($result)) {
        echo '<li>';
        echo '<input type="radio" name="doctor" value="';
        echo $row["licnum"];
        echo '">' . $row["firstname"] . " " . $row["lastname"] . "<br>";
     }
     mysqli_free_result($result);
?>
</ol>
<ol>
<?php

   echo 'Select Patient:';
   $query = 'SELECT * FROM patient';
   $result=mysqli_query($connection,$query);
    if (!$result) {
         die("database query2 failed.");
     }
    while ($row=mysqli_fetch_assoc($result)) {
        echo '<li>';
        echo '<input type="radio" name="patient" value="';
        echo $row["ohip"];
        echo '">' . $row["firstname"] . " " . $row["lastname"] . "<br>";
     }
     mysqli_free_result($result);

?>
</ol>
<input type="submit" value="Reject treatment">
</form>
<p>
<hr>

<!-- List all the doctors who are not treated by any patients -->


<p>
<h2>Doctors without patients</h2>
<?php
	include 'connecttodb.php';
	include 'doctors_no_patients.php';

?>

</body>
</html> 
